<?php
ini_set('display_errors', 0);
ini_set('max_execution_time', 300);
  // updated by pro ipogorelko@holbi.co.uk
$version = 'total_0.5.160204';
  /////////
  //Changes:
  // - crossplatform code
  /////////

//  include ('includes/application_top.php');

// connection to mysql
// settings

require ('dbset_checksum.php');

//connection to the database
$dbhandle = mysql_connect($hostname, $username, $password)
  or die("Unable to connect to MySQL");
//echo "Connected to MySQL<br>";
mysql_select_db($dbname, $dbhandle) or die('Could not select database.');
//end of code mysql connection


  /*****------------CHANGE------------*****/
  /*****-------------THIS-------------*****/
  $mailto     = 'hosting@holbi.co.uk';
  /*****------------------------------*****/
  /*****------------------------------*****/


  $extensions = array ('php', 'html', 'tpl', 'js', 'htm');
  $extensions_stats = array ('php', 'tpl', 'js', 'htm');
  $base_dir   = dirname($_SERVER['SCRIPT_FILENAME']);

  $report = '';
  $mb_hacked = false;
  $data_array = array();

  $sum_query  = mysql_query("select * from checksum_files") or die(mysql_error());
  while ($data = mysql_fetch_array($sum_query)) {
  	$data_array[$data["path"]] = $data["checksum"];
  }

	function browseDir($dirname)  {
	    global $extensions, $extensions_stats, $mb_hacked, $report, $data_array;
		$dir = opendir($dirname);
		if($dir == false)
		{
		    return;
		}
			while (($file = readdir($dir)) !== false) {
				if ($file != "." && $file != "..") {
        			if(is_file($dirname . "/" . $file) && is_readable($dirname . "/" . $file)) {
        			  $file_parts = pathinfo($dirname . "/" . $file);
				  $fcheck = substr($dirname, strrpos($dirname,"/")+1);
				  if(!array_key_exists('extension', $file_parts))
				  {
				  	continue;
				  }
        			  if (in_array($file_parts['extension'],($fcheck != 'stats' ? $extensions : $extensions_stats))) {
        			  	$mtime = filemtime($dirname . "/" . $file);
					$size = filesize($dirname . "/" . $file);
        			  	if (array_key_exists($dirname . "/" . $file,$data_array)) {
							$hash = md5($mtime.$size.substr(md5($dirname . "/" . $file),0,-2));
	                            		if ($data_array[$dirname . "/" . $file] !== $hash) {
								$mb_hacked = true;
								$report .= 'File change warning: `' . $dirname . "/" . $file . '` Date modified: `' . date("d.m.Y H:i",$mtime) . "`\n";
		                        	}
        			  	} else {
        			  		$mb_hacked = true;
        			  		$report .= 'New file warning: `' . $dirname . "/" . $file . '` Date modified: `' . date("d.m.Y H:i",$mtime) . "`\n";
        			  	}
        			  }
				}

				if(is_dir($dirname."/".$file)) {
					browseDir($dirname."/".$file);
				}
				}
			}
		closedir($dir);
	}

//  print_r('started!');
  browseDir($base_dir);

  $report .="\n";
  $report .='If you know that this is safe changes, you can update status using this link http://'.$_SERVER['HTTP_HOST']."/files_update.php\n";

  if ($mb_hacked) {
    mail($mailto, 'Holbi alert: Project has been changed ' . $_SERVER['HTTP_HOST'], $report);
    file_get_contents('https://holbihost.co.uk/noc/rep/ckvohnkm.php?hostname='. $_SERVER['HTTP_HOST'] .'&status=check&version='.$version);
#    print_r('detected!');
  }
//print_r('completed!');
?>
